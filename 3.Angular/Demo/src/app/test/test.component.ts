import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {
  id:number;
  name:string;
  avg:number;
  address:any;
  hobbies:any;

  constructor(){
    //alert("constructor invoked...");
    this.id = 101;
    this.name = 'ranith';
    this.avg = 168.13;
    this.address = {streetNo:101,city:'hyderabad',state:'telangana'};
    this.hobbies = ['sleeping','Eating','music','roaming']

  }
  ngOnInit(){
    //alert("ngOnInit invoked...");
  }

}
