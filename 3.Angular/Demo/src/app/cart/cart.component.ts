import { Component } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent {
  cartItems: any;
  cartProducts: any;
  totalAmount: number = 0;


  constructor() {
    
  }
  
  ngOnInit() {    
    this.cartProducts = localStorage.getItem("cartItems");
    this.cartItems = JSON.parse(this.cartProducts);
    this.calculateTotalAmount();
  }

  // Function to calculate the total amount
  calculateTotalAmount() {
    this.totalAmount = this.cartItems.reduce((total: number, product: any) => total + product.price, 0);
  }
}

