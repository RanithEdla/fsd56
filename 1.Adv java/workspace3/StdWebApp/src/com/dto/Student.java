package com.dto;

public class Student {
    private int StdId;
    private String StdName;
    private String Gender;
    private String emailId;
    private int age;
    private String password;
    
    public Student(){
    	super();
    }
public Student(String StdName,String Gender,String emailId,int age,String password, int StdId){
	super();
	this.StdId = StdId;
	this.StdName = StdName;
	this.Gender = Gender;
	this.emailId = emailId;
	this.age = age;
	this.password = password;
}
public int getStdId() {
	return StdId;
}
public void setStdId(int stdId) {
	StdId = stdId;
}
public String getStdName() {
	return StdName;
}
public void setStdName(String stdName) {
	StdName = stdName;
}
public String getGender() {
	return Gender;
}
public void setGender(String gender) {
	Gender = gender;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
@Override
public String toString(){
	return "Student[empId="+StdId+", StdName="+StdName+", gender="+Gender+",emailId="+emailId+", age="+age+", password="+password+"]";
}
}

